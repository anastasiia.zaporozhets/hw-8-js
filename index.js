const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(function (paragraph){
    paragraph.style.backgroundColor = '#ff0000';
});

const optionsListElement = document.getElementById('optionsList');
console.log(optionsListElement);

const optionsListParent = optionsListElement.parentElement;
console.log(optionsListParent);

const optionListChildNode = optionsListElement.childNodes;
console.log(optionListChildNode);

const testParagraphElement = document.querySelector('#testParagraph');
testParagraphElement.textContent = 'This is a paragraph';
console.log(testParagraphElement);

const mainHeaderElements = document.querySelectorAll('.main-header *');
console.log(mainHeaderElements)
mainHeaderElements.forEach(function (element){
    element.classList.add("nav-item");
    console.log(element);
})

const sectionTitleElements =document.querySelectorAll('.section-title');
sectionTitleElements.forEach(function (title){
    title.classList.remove('section-title');
})
console.log(sectionTitleElements);
