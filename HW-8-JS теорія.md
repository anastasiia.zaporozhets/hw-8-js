﻿
**1.Опишіть своїми словами що таке Document Object Model (DOM)**

Document Object Model (DOM) - це стандартна специфікація програмного інтерфейсу (API) для HTML  документів, за допомогою якої визначають структуру документа та можемо отримувати доступ до вмісту документа та взаємодіяти з ним. 

тобто це представлення веб-сторінки як дерева об'єктів, доступного для змін та взаємодії з ним за допомогою коду на JavaScript.

В JS за допомогою DOM  ми можемо отримувати доступ до всіх елементів сторінки та їх властивостей, змінювати та оновлювати їх вміст, стилі та атрибути. Наприклад, можна отримати доступ до елементу з айді  або класом та змінити його властивості, встановити нові атрибути, додати або видалити елементи на сторінці і ще багато чого корисного.

**2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?**

Властивість innerHTML та innerText використовуються для отримання або встановлення вмісту html-елемента/

innerHTML - повертає або встановлює html-код всередині елемента, включаючи теги та їх атрибути. Це означає, що можна  встановити вміст зі структурою html-коду та використовувати його для вставки елементів або стилів.

innerText - повертає або встановлює тільки текстовий вміст елемента, без будь-яких html-тегів або їх атрибутів. Тобто можна використовувати innerText для отримання або встановлення звичайного тексту в елементі, без впливу на структуру html-коду.

Якщо потрібно вставити html-код або додати нові елементи в елемент, то ми використовуємо innerHTML. А якщо потрібно отримати або встановити тільки текстовий вміст, то innerText буде кращим варіантом. 

**3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?**

Є кілька способів звернення до елемента сторінки за допомогою JS:

1\. getElementById -це метод за допомогою якого можна отримати елемент за допомогою його ідентифікатора, який повинен бути унікальним на сторінці. 

2\. За допомогою методу getElementsByClassName: Цей метод повертає масив усіх елементів з певним класом, що дозволяє легко звернутися до кількох елементів з одним класом. 

3\. getElementsByTagName: Цей метод повертає масив усіх елементів з певним тегом, дозволяючи звернутися до кількох елементів з одного тегу. Синтаксис: document.getElementsByTagName("tag");

4\. querySelector: Цей метод дозволяє знайти перший елемент на сторінці, який відповідає певному селектору CSS. 

5\. querySelectorAll: Цей метод повертає масив усіх елементів на сторінці, які відповідають певному селектору CSS. Синтаксис: document.querySelectorAll("selector");

Кращий спосіб залежить від конкретної ситуації та завдання, яке потрібно вирішити. Якщо потрібен доступ до конкретного елемента з унікальним ідентифікатором, то метод getElementById буде найкращим варіантом, якщо потрібно звернутися до кількох елементів з одним класом або тегом, то варто використовувати відповідно метод getElementsByClassName або getElementsByTagName, якщо знайти елементи за допомогою CSS-селекторів, то варто використовувати методи querySelector або querySelectorAll.


